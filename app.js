/**
 * Created by lawrence on 2/19/16.
 */
(function(){
  var express = require('express'),
      app = express(),
      http = require('http'),
      httpServer = http.createServer(app);

      app.set('http', 80);

    app.use(express.static('app'));

    app.get('/', function (res, req){
      res.render('index.html')
    });

    httpServer.on('listening', function (){
      console.log('Listening on port %d', app.get('http'));
    });
    httpServer.listen(app.get('http'));
})();
